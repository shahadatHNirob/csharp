﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassContractor
{
    class line
    {
        public double length;
        public line(double len)
        {
            Console.WriteLine("Object Created");
            length = len;
        }
        public void setLength(double len)
        {
            length = len;
        }
        public void getLength()
        {
            Console.WriteLine("The length is {0}",length);
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            line L = new line(10.0);
            Console.WriteLine(L.length);
            L.setLength(4.0);
            L.getLength();

            Console.ReadKey();

        }
    }
}
