﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConditionalLogic
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int temp = 11;
            if(temp%2==0)
            {
                Console.WriteLine("{0} :This is even number",temp);

            }
            else
            {
                Console.WriteLine("{0} : This is odd number",temp);
            }
            Console.ReadKey();
        }
    }
}
