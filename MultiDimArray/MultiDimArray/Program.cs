﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiDimArray
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[,] array =new int[4,2] { { 1, 2 }, { 3, 4 }, { 5, 6 }, { 6, 7 } };

            for(int i=0;i<4;i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    Console.WriteLine(array[i, j]);
                }
            }
            Console.ReadKey();
        }
    }
}
