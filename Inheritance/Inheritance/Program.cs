﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    class Employee
    {
        public int name;
        public int age;
        public Employee()
        {
            Console.WriteLine("Parent class's constructor");
        }
        public Employee(string s)
        {
            Console.WriteLine("Parent class's constructor"+" "+s);
        }
    }
    class Fulltime : Employee
    {
        public int salary;
        public Fulltime() :base("salary done")
        {
            Console.WriteLine("Child class's constructor");
        }

    }
    internal class Program
    {
        static void Main(string[] args)
        {
            Fulltime emp = new Fulltime();
            Console.ReadKey();
        }
    }
}
