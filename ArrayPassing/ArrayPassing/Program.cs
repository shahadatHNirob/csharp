﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayPassing
{
    class AVG
    {
        public double getAVG(int[] arr,int size)
        {
            double avg = 0,sum=0;
            for (int i = 0; i < size; i++)
            {
                sum += arr[i];
            }
            avg=sum/size;
            return avg;
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            AVG app = new AVG();
            int[] arr = new int[] { 1, 2, 3, 4, 5 };
            double ans = app.getAVG(arr, 5);
            Console.WriteLine(ans);
            Console.ReadKey();

        }
    }
}
