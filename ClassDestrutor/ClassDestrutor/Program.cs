﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassDestrutor
{
    class Line
    {
        private double x;
        public Line()
        {
            Console.WriteLine("Object has been created");
        }
        ~Line()
        {
            Console.WriteLine("Object has been deleted");
        }
        public void setVal(double temp)
        {
            x = temp;
        }
        public void getVal()
        {
            Console.WriteLine(x);
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            Line line = new Line();
            line.setVal(10.0);
            line.getVal();

            Console.ReadKey();
        }
    }
}
