﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @interface
{
    public interface IInterface
    {
        int getRes();
        void setRes(int res);
    }

    class A:IInterface
    {
        public int res = 22;
        public void setRes(int res)
        {
            res = 22;
        }
        public int getRes()
        {
            return res;
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            A a = new A();
            a.setRes(10);
            Console.WriteLine(a.getRes());
            Console.ReadKey();
        }
    }
}
