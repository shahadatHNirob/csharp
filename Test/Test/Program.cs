﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Sum
    {

        public int Getsum(int a,int b)
        {
            return a + b;
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            Sum ans = new Sum();
            string str1 = Console.ReadLine();
            int a = int.Parse(str1);
            string str2 = Console.ReadLine();
            int b = int.Parse(str2);
            Console.WriteLine(ans.Getsum(a,b));

            Console.ReadKey();
        }
    }
}
