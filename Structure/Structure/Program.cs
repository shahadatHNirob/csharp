﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structure
{
    struct Books
    {
        public string Name;
        public string Author;
        public string Subject;

        public void getValue(string n,string a,string s)
        {
            Name = n;
            Author = a;
            Subject = s;
        }
        public void Display()
        {
            Console.WriteLine("Title : {0}", Name);
            Console.WriteLine("Author : {0}", Author);
            Console.WriteLine("Subject : {0}", Subject);
        }


    };
    internal class Program
    {
        static void Main(string[] args)
        {
            Books book1 = new Books();
            Books book2 = new Books();

            book1.getValue("The Subtle Art of Not Giving a F***", "Mark Manson", "Constructive");
            book2.getValue("13 Reasons Why", "Jay Ashar", "Drama");

            book1.Display();
            book2.Display();

            Console.ReadKey();
        }
    }
}
