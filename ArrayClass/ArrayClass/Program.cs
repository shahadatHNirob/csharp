﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayClass
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] list = { 23, 52, 12, 96, 34, 15 };
            int[] temp = list;

            for (int i = 0; i < list.Length; i++)
            {
                Console.Write(list[i]+" ");
            }
            Console.WriteLine();

            Array.Sort(temp);
            for (int i = 0; i < temp.Length; i++)
            {
                Console.Write(temp[i]+" ");
            }
            Console.WriteLine();

            Array.Reverse(temp);
            for (int i = 0; i < temp.Length; i++)
            {
                Console.Write(temp[i]+ " ");
            }
            Console.WriteLine();

            Console.ReadKey();
        }
    }
}
